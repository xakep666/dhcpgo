package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

var fname string

func init() {
	flag.StringVar(&fname, "c", "config.cfg", "Path to config file")
	flag.Parse()
	//subscribe on SIGHUP for config reloading
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGHUP)
	go func() {
		s := <-sigchan
		switch s {
		case syscall.SIGHUP:
			{
				openConfFile(fname)
				logWrite("Reloaded config file: "+fname, LOG_DEBUG)
			}
		}
	}()
}

func main() {
	var (
		intaddr string
	)
	openConfFile(fname)
	logWrite("Opened config file: "+fname, LOG_DEBUG)
	switch globalConf.ListenInterface {
	case nil:
		{
			intaddr = ""
		}
	default:
		{
			addrs, _ := globalConf.ListenInterface.Addrs()
			for _, v := range addrs {
				if len(v.String()) <= 15 && len(v.String()) >= 7 {
					intaddr = v.String()
					intaddr = intaddr[:len(intaddr)-3]
					break
				}
			}
		}
	}
	bcast_addr, _ := net.ResolveUDPAddr("udp4", intaddr+":"+strconv.Itoa(SERVER_PORT))
	bcast_client, _ := net.ResolveUDPAddr("udp4", "255.255.255.255:"+strconv.Itoa(CLIENT_PORT))
	bcast_socket, err := net.ListenUDP("udp4", bcast_addr)
	if err != nil || bcast_socket == nil {
		logWrite(err.Error(), LOG_ERROR)
		os.Exit(1)
	}
	logWrite("Established listening socket to port "+intaddr+":"+strconv.Itoa(SERVER_PORT), LOG_INFO)
	for {
		data := make(RawPacket, 4096)
		read, _, err := bcast_socket.ReadFromUDP(data)
		if err != nil {
			logWrite(err.Error(), LOG_ERROR)
		}
		logWrite("Read: "+strconv.Itoa(read)+" bytes", LOG_DEBUG)
		req_packet := data.toDhcp()
		for _, v := range req_packet.opts {
			if v.code == 53 { //identify msgtype
				switch DhcpMsg(v.data[0]) {
				case DHCPDISCOVER:
					{
						logWrite("Got DHCPDISCOVER from "+req_packet.chaddr.String(), LOG_DEBUG)
						snd_packet := req_packet.generateAnswer(DHCPOFFER)
						snd_buf := snd_packet.toRaw()
						send, err := bcast_socket.WriteToUDP(snd_buf, bcast_client)
						logWrite("Sending "+strconv.Itoa(send)+" bytes to "+req_packet.chaddr.String(), LOG_DEBUG)
						if err != nil {
							logWrite(err.Error(), LOG_ERROR)
						}
					}
				case DHCPREQUEST:
					{
						logWrite("Got DHCPREQUEST from "+req_packet.chaddr.String(), LOG_DEBUG)
					}
				}
				break
			}
		}
		fmt.Printf("%v\n", req_packet)
	}
}
