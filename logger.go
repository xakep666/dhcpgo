package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

const (
	LOG_ERROR int = 0 + iota
	LOG_INFO
	LOG_DEBUG
)

func EarlyError(err error) {
	fmt.Println("Early error: " + err.Error())
	os.Exit(1)
}

func logWrite(msg string, level int) {
	var (
		w        io.Writer
		levelStr string
	)
	if !globalConf.IsOpened {
		fmt.Println("Files Not Opened")
		os.Exit(1)
	}
	switch globalConf.LogFile {
	case "stdout":
		w = os.Stdout
	case "syslog":
		w = globalConf.SyslogObj
	default:
		w = globalConf.LogFileDesc
	}
	log.SetOutput(w)
	if level <= globalConf.LogLevel {
		switch level {
		case 0:
			levelStr = "ERROR"
		case 1:
			levelStr = "INFO "
		case 2:
			levelStr = "DEBUG"
		}
		log.Printf("| %s | %s\n",
			levelStr,
			msg)
	}
}
