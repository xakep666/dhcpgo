package main

import (
	"encoding/binary"
	"encoding/hex"
	//"fmt"
	"net"
	"strconv"
)

type DhcpMsg byte
type Direction byte
type Htype byte
type Flags struct {
	broadcast byte
}
type RawPacket []byte
type OptList []byte

const ( //Message types
	DHCPDISCOVER DhcpMsg = 1 + iota
	DHCPOFFER
	DHCPREQUEST
	DHCPACK
	DHCPNACK
	DHCPDECLINE
	DHCPRELEASE
	DHCPINFORM
)

const ( //Message direction
	BOOTREQUEST Direction = 1 + iota
	BOOTREPLY
)

const (
	ETHERHET Htype = 1 + iota
	EXP_ETHERNET
	AX25
	PRONET
	CHAOS
	IEEE802
	ARCNET
	HYPERCHAN
	LANSTAR
	AUTONETSHADDR
	LOCALTALK
	LOCALNET
)

const (
	SNAMESIZE  = 64
	FILESIZE   = 128
	PACKETSIZE = 812
	OPTSSIZE   = 576
)

type Option struct {
	code   byte
	optlen byte
	data   []byte
}

type DhcpPacket struct {
	op     Direction
	htype  Htype
	hlen   byte
	steps  byte
	xid    uint32
	secs   uint16
	flags  Flags
	ciaddr net.IP
	yiaddr net.IP
	giaddr net.IP
	chaddr net.HardwareAddr
	sname  string
	file   string
	opts   []Option
}

const (
	SERVER_PORT = 67
	CLIENT_PORT = 68
)

func (p DhcpPacket) toRaw() (rp RawPacket) {
	logWrite("Generating raw packet for "+p.chaddr.String(), LOG_DEBUG)
	rp = make(RawPacket, PACKETSIZE) //max packet size 809 bytes
	//copy fields to array
	rp[0] = byte(p.op)
	rp[1] = byte(p.htype)
	rp[2] = p.hlen
	rp[3] = p.steps
	binary.LittleEndian.PutUint32(rp[4:], p.xid)
	binary.LittleEndian.PutUint16(rp[8:], p.secs)

	//flags field
	rp[10] = (p.flags.broadcast & 0x1 << 4)
	rp[11] = 0x0

	copy(rp[12:], p.ciaddr.To4())
	copy(rp[16:], p.yiaddr.To4())
	copy(rp[20:], p.giaddr.To4())
	copy(rp[28:28+p.hlen], p.chaddr)
	copy(rp[40:], []byte(p.sname))
	copy(rp[104:], []byte(p.file))
	//magic cookies
	rp[236] = 99
	rp[237] = 130
	rp[238] = 83
	rp[239] = 99
	i := 0
	for _, v := range p.opts {
		rp[240+i] = v.code
		i++
		rp[240+i] = v.optlen
		i++
		copy(rp[240+i:], []byte(v.data))
		i += int(v.optlen)
	}
	return
}

func (rp RawPacket) toDhcp() (dp DhcpPacket) {
	dp.op = Direction(rp[0])
	logWrite("Decoded direction "+strconv.Itoa(int(dp.op)), LOG_DEBUG)
	dp.htype = Htype(rp[1])
	logWrite("Decoded HwType "+strconv.Itoa(int(dp.htype)), LOG_DEBUG)
	dp.hlen = byte(rp[2])
	logWrite("Decoded HwLen "+strconv.Itoa(int(dp.hlen)), LOG_DEBUG)
	dp.steps = byte(rp[3])
	logWrite("Decoded Steps "+strconv.Itoa(int(dp.steps)), LOG_DEBUG)
	dp.xid = binary.LittleEndian.Uint32(rp[4:])
	logWrite("Decoded XID 0x"+hex.EncodeToString(rp[4:8]), LOG_DEBUG)
	dp.secs = binary.LittleEndian.Uint16(rp[8:])
	logWrite("Decoded Secs "+strconv.Itoa(int(dp.secs)), LOG_DEBUG)
	dp.flags.broadcast = rp[10] >> 4
	logWrite("Decoded Broadcast flag 0x"+strconv.Itoa(int(dp.flags.broadcast)), LOG_DEBUG)
	dp.ciaddr = net.IPv4(rp[12], rp[13], rp[14], rp[15])
	logWrite("Decoded ciaddr "+dp.ciaddr.String(), LOG_DEBUG)
	dp.yiaddr = net.IPv4(rp[16], rp[17], rp[18], rp[19])
	logWrite("Decoded yiaddr "+dp.yiaddr.String(), LOG_DEBUG)
	dp.giaddr = net.IPv4(rp[20], rp[21], rp[22], rp[23])
	logWrite("Decoded giaddr "+dp.giaddr.String(), LOG_DEBUG)
	dp.chaddr = make(net.HardwareAddr, dp.hlen)
	copy(dp.chaddr, rp[28:28+dp.hlen])
	logWrite("Decoded Client MAC "+dp.chaddr.String(), LOG_DEBUG)
	dp.sname = string(rp[40:103])
	logWrite("Decoded sname "+dp.sname, LOG_DEBUG)
	dp.file = string(rp[104:231])
	logWrite("Decoded file "+dp.file, LOG_DEBUG)
	dp.opts = make([]Option, 63) //RFC2132 defines 63 options
	for i, j := 0, 0; j < 63 && i < OPTSSIZE; j++ {
		dp.opts[j].code = rp[240+i]
		logWrite("Decoded option ["+strconv.Itoa(j)+"] code: "+strconv.Itoa(int(dp.opts[j].code)), LOG_DEBUG)
		if dp.opts[j].code == 255 {
			break
		}
		i++
		dp.opts[j].optlen = rp[240+i]
		logWrite("Decoded option ["+strconv.Itoa(j)+"] length: "+strconv.Itoa(int(dp.opts[j].optlen)), LOG_DEBUG)
		i++
		dp.opts[j].data = make([]byte, dp.opts[j].optlen)
		copy(dp.opts[j].data, rp[240+i:240+i+int(dp.opts[j].optlen)])
		logWrite("Decoded option ["+strconv.Itoa(j)+"] data: 0x"+hex.EncodeToString(dp.opts[j].data), LOG_DEBUG)
		i += int(dp.opts[j].optlen)
	}
	return
}

func (dp *DhcpPacket) addOption(code byte, data []byte) {
	tmp := Option{code, byte(cap(data)), data}
	dp.opts = append(dp.opts, tmp)
}

func (q *DhcpPacket) generateAnswer(msgtype DhcpMsg) (a DhcpPacket) {
	a.op = BOOTREPLY
	a.htype = q.htype
	a.hlen = q.hlen
	a.steps = q.steps + 1
	a.secs = 0
	a.ciaddr = net.IPv4(0, 0, 0, 0)
	a.xid = q.xid
	a.flags = q.flags
	switch msgtype {
	case DHCPOFFER:
		{
			a.yiaddr = leasesBase.addLease(q.chaddr)
			logWrite("Generating DHCPOFFER answer for "+q.chaddr.String(), LOG_DEBUG)
		}
	case DHCPACK:
		{
			a.yiaddr = leasesBase.addLease(q.chaddr)
			logWrite("Generating DHCPACK answer for "+q.chaddr.String(), LOG_DEBUG)
		}
	case DHCPNACK:
		{
			a.yiaddr = net.IPv4(0, 0, 0, 0)
			logWrite("Generating DHCPNACK answer for "+q.chaddr.String(), LOG_DEBUG)
		}
	}
	a.giaddr = q.giaddr
	a.chaddr = q.chaddr
	//TODO: add sname processing
	a.file = globalConf.BootFile
	a.addOption(53, []byte{byte(msgtype)})
	for _, v := range q.opts {
		//find "parameter requested list" option
		if v.code == 55 {
			//append requested options
			for _, v1 := range globalConf.DhcpOpts {
				a.addOption(v1.Code, v1.Data)
			}
			break
		}
	}
	//add end options mark
	a.addOption(255, nil)
	return
}
