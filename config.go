package main

import (
	"encoding/binary"
	"encoding/json"
	"log/syslog"
	"net"
	"os"
	"strconv"
)

type OptConfig struct {
	Code byte
	Data []string
}

type ClientConfig struct {
	MAC string
	IP  string
}

type ClientBytes struct {
	MAC net.HardwareAddr
	IP  net.IP
}

type OptBytes struct {
	Code byte
	Data []byte
}

type GlogalCfg struct {
	ClientPool      [2]net.IP
	LogLevel        int
	LogFile         string
	LeaseFileDesc   *os.File
	LogFileDesc     *os.File
	SyslogObj       *syslog.Writer
	ListenInterface *net.Interface
	DhcpOpts        []OptBytes
	ClientIP        []ClientBytes
	BootFile        string
	IsOpened        bool
}

type Configuration struct {
	ClientPool      [2]string      `json:"clientPool"` //first - start addr, second - end addr
	LogLevel        int            `json:"logLevel"`   //0 - only error, 1 - error and info, 2 - error,info,debug
	LeaseFile       string         `json:"leaseFile"`
	LogFile         string         `json:"logFile"`              //may be also "stdout" or "syslog"
	SyslogAddr      string         `json:"syslogAddr,omitempty"` //if not set,used localhost
	ListenInterface string         `json:"listenInterface,omitempty"`
	DhcpOpts        []OptConfig    `json:"dhcpOpts,omitempty"`
	BootFile        string         `json:"bootFile,omitempty"`
	ClientIP        []ClientConfig `json:"clientIP,omitempty"` //MAC->IP
}

var globalConf GlogalCfg
var FixedOpts = OptList{0, 255, 50, 53, 55, 61}

func (list OptList) isPresentOpt(code byte) bool {
	for _, v := range list {
		if v == code {
			return true
		}
	}
	return false
}

func openConfFile(fname string) {
	var (
		opt OptBytes
		ip  net.IP
		mac net.HardwareAddr
		cfg Configuration
	)
	file, err := os.Open(fname)
	if err != nil {
		EarlyError(err)
	}
	err = json.NewDecoder(file).Decode(&cfg)
	if err != nil {
		EarlyError(err)
	}
	switch cfg.LogFile {
	case "syslog":
		if cfg.SyslogAddr != "" {
			globalConf.SyslogObj, err = syslog.Dial("udp", cfg.SyslogAddr, syslog.LOG_DAEMON, "GoDHCP")
		} else {
			globalConf.SyslogObj, err = syslog.New(syslog.LOG_DAEMON, "GoDHCP")
		}
		if err != nil {
			EarlyError(err)
		}
	case "stdout":
		{
		}
	default:
		globalConf.LogFileDesc, err = os.OpenFile(cfg.LogFile, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
		if err != nil {
			EarlyError(err)
		}
	}
	globalConf.LeaseFileDesc, err = os.OpenFile(cfg.LeaseFile, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		EarlyError(err)
	}
	for k, v := range cfg.ClientPool {
		globalConf.ClientPool[k] = net.ParseIP(v)
	}
	globalConf.LogLevel = cfg.LogLevel
	for _, v := range cfg.DhcpOpts {
		if FixedOpts.isPresentOpt(v.Code) {
			continue
		}
		opt.Code = v.Code
		for _, v1 := range v.Data {
			ip = net.ParseIP(v1)
			if ip != nil { //check if it IP address
				ip = ip.To4()
				for _, v2 := range []byte(ip) {
					opt.Data = append(opt.Data, v2)
				}
			} else {
				mac, err = net.ParseMAC(v1)
				if err == nil { //check if it MAC address
					for _, v2 := range []byte(mac) {
						opt.Data = append(opt.Data, v2)
					}
				} else {
					tmp, err := strconv.Atoi(v1) //check if it is number
					if err == nil {
						buf := make([]byte, 8)
						binary.PutVarint(buf, int64(tmp))
						for _, v2 := range buf {
							opt.Data = append(opt.Data, v2)
						}
					} else { //will append as string
						for _, v2 := range []byte(v1) {
							opt.Data = append(opt.Data, v2)
						}
					}
				}
			}

		}
		globalConf.DhcpOpts = append(globalConf.DhcpOpts, opt)
		opt.Data = nil
	}
	if cfg.ListenInterface != "" {
		globalConf.ListenInterface, err = net.InterfaceByName(cfg.ListenInterface)
		if err != nil {
			EarlyError(err)
		}
	}
	globalConf.LogFile = cfg.LogFile
	for _, v := range cfg.ClientIP {
		mac, err = net.ParseMAC(v.MAC)
		if err != nil {
			EarlyError(err)
		}
		ip = net.ParseIP(v.IP)
		globalConf.ClientIP = append(globalConf.ClientIP, ClientBytes{mac, ip})
	}
	globalConf.BootFile = cfg.BootFile
	globalConf.IsOpened = true
}
