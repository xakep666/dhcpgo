package main

import (
	//	"os"
	"encoding/binary"
	"net"
)

type lease struct {
	mac net.HardwareAddr
	ip  net.IP
}

type leases []lease

var leasesBase leases

func (l *leases) getLeaseMac(ip net.IP) net.HardwareAddr {
	for _, v := range *l {
		if ip.Equal(v.ip) {
			return v.mac
		}
	}
	return nil
}

func (l *leases) getLeaseIP(mac net.HardwareAddr) net.IP {
	for _, v := range *l {
		if mac.String() == v.mac.String() {
			return v.ip
		}
	}
	return nil
}

func (l *leases) addLease(mac net.HardwareAddr) net.IP {
	//if lease found, return its ip
	found := l.getLeaseIP(mac)
	if found != nil {
		return found
	}
	//check if possible assign ip from config
	for _, v := range globalConf.ClientIP {
		if v.MAC.String() == mac.String() && l.getLeaseIP(mac) == nil {
			tmp := lease{mac, v.IP}
			*l = append(*l, tmp)
			return v.IP
		}
	}
	//make int from ip
	ip := make([]byte, 4)
	for a := binary.LittleEndian.Uint16([]byte(globalConf.ClientPool[0].To4())); a <= binary.LittleEndian.Uint16([]byte(globalConf.ClientPool[1].To4())); a++ {
		binary.LittleEndian.PutUint16(ip, a)
		if l.getLeaseMac(net.IP(ip)) == nil {
			tmp := lease{mac, net.IP(ip)}
			*l = append(*l, tmp)
			return net.IP(ip)
		}
	}
	return nil
}

func (l *leases) delLease(ip net.IP) {
	tmp := *l
	for k, v := range tmp {
		if v.ip.Equal(ip) {
			tmp = tmp[:k+copy(tmp[k:], tmp[:k+1])]
		}
	}
	*l = tmp
}
